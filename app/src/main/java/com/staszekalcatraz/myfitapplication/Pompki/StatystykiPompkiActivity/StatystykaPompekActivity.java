package com.staszekalcatraz.myfitapplication.Pompki.StatystykiPompkiActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.staszekalcatraz.myfitapplication.DatabaseHandler.DatabaseHandler;
import com.staszekalcatraz.myfitapplication.R;

import java.util.ArrayList;

public class StatystykaPompekActivity extends AppCompatActivity {

    private BarChart barChartPompki;

    private LineChart lineChartPompki;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statystyka_pompek);

        DatabaseHandler databaseHandler = new DatabaseHandler(this);
        int pompkiRecordsInDatabse = databaseHandler.getPompkiCount();

        lineChartPompki = findViewById(R.id.linearChartPompki);

        //lineChartPompki.setOnChartGestureListener(this);
        //lineChartPompki.setOnChartValueSelectedListener(this);

        lineChartPompki.setDragEnabled(true);
        lineChartPompki.setScaleEnabled(false);

        ArrayList<Entry> yValues = new ArrayList<>();
        ArrayList<String> mDates = new ArrayList<>();

        for (int i = 1; i <= pompkiRecordsInDatabse; i++) {
            yValues.add(new Entry(i, databaseHandler.getSingleArchiveTimeRecord(i).getPowtorzeniaCwiczenia()));

            // mDates.add(i, databaseHandler.getSingleArchiveTimeRecord(i).getDataCwiczenia());
            Log.e("tag", "i: " + i + " data cwiczenia: " + databaseHandler.getSingleArchiveTimeRecord(i).getDataCwiczenia());
        }

        //XAxis xAxis = barChartPompki.getXAxis();
        //xAxis.setValueFormatter(new MyAxisValueFormatter(mDates));
        //xAxis.setGranularity(1f);

        LineDataSet lineDataSet = new LineDataSet(yValues, "Pompki");
        lineDataSet.setColor(Color.RED);
        lineDataSet.setLineWidth(3);
        lineDataSet.setValueTextSize(10);
        lineDataSet.setFillAlpha(110);

        ArrayList<ILineDataSet> dataSets = new ArrayList<>();
        dataSets.add(lineDataSet);

        LineData data = new LineData(dataSets);
        lineChartPompki.setData(data);

        YAxis mLeftAxis = lineChartPompki.getAxisLeft();
        mLeftAxis.removeAllLimitLines();

//------------------
        /*barChartPompki = findViewById(R.id.barChartPompki);
        barChartPompki.setDrawBarShadow(false);
        barChartPompki.setDrawValueAboveBar(true);
        barChartPompki.getDescription().setEnabled(false);

        //mChart.setMaxVisibleValueCount(60);

        DatabaseHandler databaseHandler = new DatabaseHandler(this);
        int pompkiRecordsInDatabse = databaseHandler.getPompkiCount();

        ArrayList<BarEntry> barEntries = new ArrayList<>();
        ArrayList<String> theDates = new ArrayList<>();

        for (int i = 1; i <= pompkiRecordsInDatabse; i++) {
            barEntries.add(new BarEntry(databaseHandler.getSingleArchiveTimeRecord(i).getId(), databaseHandler.getSingleArchiveTimeRecord(i).getPowtorzeniaCwiczenia()));
            theDates.add(databaseHandler.getSingleArchiveTimeRecord(i).getDataCwiczenia());
            Log.e("i", "" + i);
        }

        BarDataSet barDataSet = new BarDataSet(barEntries, "Pompki");

        for (int i = 1; i <= pompkiRecordsInDatabse; i++) {

            Log.e("daty", "" + databaseHandler.getSingleArchiveTimeRecord(i).getDataCwiczenia());
        }

        BarData barData = new BarData(barDataSet);
        barChartPompki.setData(barData);

        XAxis xAxis = barChartPompki.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setCenterAxisLabels(true);
        xAxis.setDrawGridLines(false);
        //xAxis.setGranularity(1f); // only intervals of 1 day
        xAxis.setLabelCount(7);
        xAxis.setValueFormatter(new MyAxisValueFormatter(theDates));

        //https://www.youtube.com/watch?v=naPRHNfzDk8&t=742s*/
    }

    public class MyAxisValueFormatter implements IAxisValueFormatter {

        private ArrayList<String> mValues;

        public MyAxisValueFormatter(ArrayList<String> values) {
            this.mValues = values;
        }

        @Override
        public String getFormattedValue(float value, AxisBase axis) {
            return mValues.get((int) value);
        }

    }
}
