package com.staszekalcatraz.myfitapplication.Pompki.Przypominajka;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.Toast;

import com.rengwuxian.materialedittext.MaterialEditText;
import com.staszekalcatraz.myfitapplication.Pompki.PompkiActivity;
import com.staszekalcatraz.myfitapplication.R;

import java.util.Calendar;

public class PrzypominajkaPompkiActivity extends AppCompatActivity {
    private ScheduleClient scheduleClient;
    private DatePicker picker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_przypominajka_pompki);

        //obiekt serwisu w tle
        scheduleClient = new ScheduleClient(this);
        scheduleClient.doBindService();

        picker = findViewById(R.id.scheduleTimePicker);
    }

    //obsluga daty i godziny powiadomienia
    public void onDateSelectedButtonClick(View v) {
        // Get the date from our datepicker
        int day = picker.getDayOfMonth();
        int month = picker.getMonth();
        int year = picker.getYear();
        Calendar c = Calendar.getInstance();

        MaterialEditText etGodzina = findViewById(R.id.etGodzina);
        MaterialEditText etMinuty = findViewById(R.id.etMinuty);

        //algo
        if (!etGodzina.getText().toString().isEmpty()) {
            if (etMinuty.getText().toString().isEmpty()) {
                etMinuty.setText("00");
            }
            if (Integer.parseInt(etGodzina.getText().toString()) > 25) {
                Toast.makeText(this, R.string.podaj_prawidlowa_godzine, Toast.LENGTH_SHORT).show();

                return;
            }
            c.set(year, month, day);
            c.set(Calendar.HOUR_OF_DAY, Integer.parseInt(etGodzina.getText().toString()));
            c.set(Calendar.MINUTE, Integer.parseInt(etMinuty.getText().toString()));
            c.set(Calendar.SECOND, 0);

            // Ask our service to set an alarm for that date, this activity talks to the client that talks to the service
            scheduleClient.setAlarmForNotification(c);
            // Notify the user what they just did
            Toast.makeText(this, "Ustawiono powiadomienie na: " + day + "/" + (month + 1) + "/" + year + "\nGodzina: " +
                    etGodzina.getText().toString() + ":" + etMinuty.getText().toString(), Toast.LENGTH_SHORT).show();

            startActivity(new Intent(PrzypominajkaPompkiActivity.this, PompkiActivity.class));
        } else {
            Toast.makeText(this, R.string.podaj_prawidlowa_godzine, Toast.LENGTH_SHORT).show();

        }
    }

    @Override
    protected void onStop() {
        // When our activity is stopped ensure we also stop the connection to the service
        // this stops us leaking our activity into the system *bad*
        if (scheduleClient != null)
            scheduleClient.doUnbindService();
        super.onStop();
    }

}
