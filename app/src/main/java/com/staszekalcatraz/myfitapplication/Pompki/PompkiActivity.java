package com.staszekalcatraz.myfitapplication.Pompki;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.rengwuxian.materialedittext.MaterialEditText;
import com.staszekalcatraz.myfitapplication.DatabaseHandler.CwiczenieModel;
import com.staszekalcatraz.myfitapplication.DatabaseHandler.DatabaseHandler;
import com.staszekalcatraz.myfitapplication.Pompki.Przypominajka.PrzypominajkaPompkiActivity;
import com.staszekalcatraz.myfitapplication.Pompki.StatystykiPompkiActivity.StatystykaPompekActivity;
import com.staszekalcatraz.myfitapplication.R;

import java.text.DateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PompkiActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    @BindView(R.id.tvLiczbaPowtorzen)
    TextView tvLiczbaPowtorzen;

    @BindView(R.id.tvDzien)
    TextView tvDzien;

    @BindView(R.id.tvTimerAndPushups)
    TextView tvTimerAndPushups;

    @BindView(R.id.tvInfo)
    TextView tvInfo;

    @BindView(R.id.bEndSerie)
    Button bEndSerie;

    @BindView(R.id.bStart)
    Button bStart;

    private int liczbaPompek;

    private int liczbaUkonczonychPompek = 0;

    private int seconds = 6;

    private CountDownTimer countDownTimer;

    private DatabaseHandler databaseHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pompki);
        ButterKnife.bind(this);

        databaseHandler = new DatabaseHandler(this);

        SharedPreferences sharedLiczbaPowtorzen = getSharedPreferences("liczbaPowtorzenPompek", Context.MODE_PRIVATE);
        //sprawdzamy czy uzytkownik wpisal juz swoj maks, jezeli nie to wyswietla okienko zeby wpisal
        String liczbaPowtorzen = sharedLiczbaPowtorzen.getString("liczbaPowtorzenPompek", "");
        Log.e("liczbaPowtorzen", "" + liczbaPowtorzen);
        if (liczbaPowtorzen.equals("")) {
            onZmienLiczbePompekClicked();
        } else {
            tvLiczbaPowtorzen.setText(liczbaPowtorzen);
        }
    }

    @OnClick(R.id.bZmienLiczbePompek)
    public void onZmienLiczbePompekClicked() {
        AlertDialog.Builder alertLiczbaPowtorzen = new AlertDialog.Builder(this);
        alertLiczbaPowtorzen.setTitle("Podaj liczbę maksymalnych powtórzeń");

        //ustalamy layout dla okna
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        View liczbaPompekLayout = layoutInflater.inflate(R.layout.dialog_liczba_pompek, null);

        final MaterialEditText etLiczbaPompek = liczbaPompekLayout.findViewById(R.id.etLiczbaPowtorzen);

        alertLiczbaPowtorzen.setView(liczbaPompekLayout);
        alertLiczbaPowtorzen.setPositiveButton("Zatwierdz", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();

                String liczbaPowtorzen = etLiczbaPompek.getText().toString();

                SharedPreferences sharedLiczbaPowtorzen = getSharedPreferences("liczbaPowtorzenPompek", Context.MODE_PRIVATE);
                SharedPreferences.Editor sharedPrefEditor = sharedLiczbaPowtorzen.edit();
                sharedPrefEditor.putString("liczbaPowtorzenPompek", liczbaPowtorzen);
                sharedPrefEditor.apply();

                tvLiczbaPowtorzen.setText(liczbaPowtorzen);
            }
        }).setNegativeButton(R.string.odrzuc, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        alertLiczbaPowtorzen.create().show();
    }

    @OnClick(R.id.bPrzypominajka)
    public void onPrzypominajkaClick() {
        startActivity(new Intent(PompkiActivity.this, PrzypominajkaPompkiActivity.class));
    }

    @OnClick(R.id.bStart)
    public void onStartButtonClicked() {
        AlertDialog.Builder dzienDialog = new AlertDialog.Builder(this);
        dzienDialog.setTitle(R.string.wybierz_dzien);

        //ustalamy layout dla okna
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        View workoutDayLayout = layoutInflater.inflate(R.layout.dialog_workout_day, null);

        //deklaracja spinnera
        Spinner daysSpinner = workoutDayLayout.findViewById(R.id.daysSpinner);
        daysSpinner.setOnItemSelectedListener(this);

        // wyglad spinnera(ozwijanego menu) i nadanie mu skad ma pobierac dane
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.days_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // zapis zawartosci rozwijanego menu z dniami
        daysSpinner.setAdapter(adapter);

        dzienDialog.setView(workoutDayLayout);

        dzienDialog.setPositiveButton(R.string.zatwierdz, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();

            }
        });

        dzienDialog.create().show();
    }

    @OnClick(R.id.bStatystykaPompek)
    public void onStatystykaPompekButtonClicked() {
        startActivity(new Intent(PompkiActivity.this, StatystykaPompekActivity.class));
    }
    /*//koniec serii, odpal timer
    //@OnClick(R.id.bEndSerie)
    public void onButtonEndSerieClick() {
        CountDownTimer countDownTimer = new CountDownTimer(seconds * 1000, 1000) {
            @Override
            public void onTick(long l) {
                tvTimerAndPushups.setText("" + (int) (l / 1000));
                bEndSerie.setVisibility(View.GONE);
            }

            @Override
            public void onFinish() {
                int i = 1;
                i++;
                bEndSerie.setVisibility(View.VISIBLE);
            }
        }.start();
    }*/

    //po wyborze dnia, przypisujemy nr dnia
    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
        int dzien;
        tvDzien.setText(String.valueOf(adapterView.getItemAtPosition(position)));

        int liczbaPompekwSerii = 1;

        switch (position) {
            case 0:
                dzien = 1;

                break;
            case 1:
                dzien = 2;

                break;
            case 2:
                dzien = 3;

                break;
            case 3:
                dzien = 4;

                break;
            case 4:
                dzien = 5;

                break;
            case 5:
                dzien = 6;

                break;

            default:
                dzien = 1;

                break;
        }
        startWorkoutSeriaPierwsza(dzien, liczbaPompekwSerii);
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
    }

    private void startWorkoutSeriaPierwsza(final int dzien, int liczbaPompekwSerii) {
        tvInfo.setText("Wykonaj:");
        bStart.setVisibility(View.GONE);
        SharedPreferences sharedLiczbaPowtorzen = getSharedPreferences("liczbaPowtorzenPompek", Context.MODE_PRIVATE);
        liczbaPompek = Integer.parseInt(sharedLiczbaPowtorzen.getString("liczbaPowtorzenPompek", ""));
        if (liczbaPompek <= 5) {
            switch (dzien) {
                case 1:
                    liczbaPompekwSerii = 2;
                    bEndSerie.setVisibility(View.VISIBLE);
                    bEndSerie.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            countDownTimer = new CountDownTimer(seconds * 1000, 1000) {
                                @Override
                                public void onTick(long l) {
                                    tvInfo.setText("Odpocznij");

                                    tvTimerAndPushups.setText("" + (int) (l / 1000));
                                    tvTimerAndPushups.setTextColor(Color.GREEN);
                                    bEndSerie.setVisibility(View.GONE);
                                }

                                @Override
                                public void onFinish() {
                                    bEndSerie.setVisibility(View.VISIBLE);
                                    startWorkoutSeriaDwa(dzien, 3);

                                }
                            }.start();
                        }
                    });
                    liczbaUkonczonychPompek += liczbaPompekwSerii;

                    break;

                case 2:
                    liczbaPompekwSerii = 3;
                    bEndSerie.setVisibility(View.VISIBLE);
                    bEndSerie.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            countDownTimer = new CountDownTimer(seconds * 1000, 1000) {
                                @Override
                                public void onTick(long l) {
                                    tvInfo.setText("Odpocznij");

                                    tvTimerAndPushups.setText("" + (int) (l / 1000));
                                    tvTimerAndPushups.setTextColor(Color.GREEN);
                                    bEndSerie.setVisibility(View.GONE);
                                }

                                @Override
                                public void onFinish() {
                                    bEndSerie.setVisibility(View.VISIBLE);
                                    startWorkoutSeriaDwa(dzien, 3);

                                }
                            }.start();
                        }
                    });
                    liczbaUkonczonychPompek += liczbaPompekwSerii;

                    break;


                case 3:
                    liczbaPompekwSerii = 4;
                    bEndSerie.setVisibility(View.VISIBLE);
                    bEndSerie.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            countDownTimer = new CountDownTimer(seconds * 1000, 1000) {
                                @Override
                                public void onTick(long l) {
                                    tvInfo.setText("Odpocznij");

                                    tvTimerAndPushups.setText("" + (int) (l / 1000));
                                    tvTimerAndPushups.setTextColor(Color.GREEN);
                                    bEndSerie.setVisibility(View.GONE);
                                }

                                @Override
                                public void onFinish() {
                                    bEndSerie.setVisibility(View.VISIBLE);
                                    startWorkoutSeriaDwa(dzien, 3);

                                }
                            }.start();
                        }
                    });
                    liczbaUkonczonychPompek += liczbaPompekwSerii;

                    break;

                case 4:
                    liczbaPompekwSerii = 5;
                    bEndSerie.setVisibility(View.VISIBLE);
                    bEndSerie.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            countDownTimer = new CountDownTimer(seconds * 1000, 1000) {
                                @Override
                                public void onTick(long l) {
                                    tvInfo.setText("Odpocznij");

                                    tvTimerAndPushups.setText("" + (int) (l / 1000));
                                    tvTimerAndPushups.setTextColor(Color.GREEN);
                                    bEndSerie.setVisibility(View.GONE);
                                }

                                @Override
                                public void onFinish() {
                                    bEndSerie.setVisibility(View.VISIBLE);
                                    startWorkoutSeriaDwa(dzien, 3);

                                }
                            }.start();
                        }
                    });
                    liczbaUkonczonychPompek += liczbaPompekwSerii;

                    break;

                case 5:
                    liczbaPompekwSerii = 5;
                    bEndSerie.setVisibility(View.VISIBLE);
                    bEndSerie.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            countDownTimer = new CountDownTimer(seconds * 1000, 1000) {
                                @Override
                                public void onTick(long l) {
                                    tvInfo.setText("Odpocznij");

                                    tvTimerAndPushups.setText("" + (int) (l / 1000));
                                    tvTimerAndPushups.setTextColor(Color.GREEN);
                                    bEndSerie.setVisibility(View.GONE);
                                }

                                @Override
                                public void onFinish() {
                                    bEndSerie.setVisibility(View.VISIBLE);
                                    startWorkoutSeriaDwa(dzien, 3);

                                }
                            }.start();
                        }
                    });
                    liczbaUkonczonychPompek += liczbaPompekwSerii;

                    break;
                case 6:
                    liczbaPompekwSerii = 5;
                    bEndSerie.setVisibility(View.VISIBLE);
                    bEndSerie.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            countDownTimer = new CountDownTimer(seconds * 1000, 1000) {
                                @Override
                                public void onTick(long l) {
                                    tvInfo.setText("Odpocznij");

                                    tvTimerAndPushups.setText("" + (int) (l / 1000));
                                    tvTimerAndPushups.setTextColor(Color.GREEN);
                                    bEndSerie.setVisibility(View.GONE);
                                }

                                @Override
                                public void onFinish() {
                                    bEndSerie.setVisibility(View.VISIBLE);
                                    startWorkoutSeriaDwa(dzien, 3);

                                }
                            }.start();
                        }
                    });
                    liczbaUkonczonychPompek += liczbaPompekwSerii;

                    break;
            }
        } else if (liczbaPompek > 5 && liczbaPompek <= 10) {

        } else if (liczbaPompek > 10 && liczbaPompek <= 20) {

        } else if (liczbaPompek > 20 && liczbaPompek <= 25) {

        } else if (liczbaPompek > 25 && liczbaPompek <= 30) {

        } else if (liczbaPompek > 30 && liczbaPompek <= 35) {

        } else if (liczbaPompek > 40 && liczbaPompek <= 45) {

        } else if (liczbaPompek > 45 && liczbaPompek <= 50) {

        } else if (liczbaPompek > 50 && liczbaPompek <= 55) {

        } else if (liczbaPompek > 55 && liczbaPompek <= 60) {

        } else if (liczbaPompek > 60) {

        }
        tvTimerAndPushups.setText("" + liczbaPompekwSerii);
    }

    private void startWorkoutSeriaDwa(final int dzien, int liczbaPompekwSerii) {
        tvInfo.setText("Wykonaj:");
        tvTimerAndPushups.setText("" + liczbaPompekwSerii);

        bEndSerie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                countDownTimer = new CountDownTimer(seconds * 1000, 1000) {
                    @Override
                    public void onTick(long l) {
                        tvInfo.setText("Odpocznij");

                        tvTimerAndPushups.setText("" + (int) (l / 1000));
                        tvTimerAndPushups.setTextColor(Color.GREEN);
                        bEndSerie.setVisibility(View.GONE);
                    }

                    @Override
                    public void onFinish() {
                        bEndSerie.setVisibility(View.VISIBLE);
                        SharedPreferences sharedLiczbaPowtorzen = getSharedPreferences("liczbaPowtorzenPompek", Context.MODE_PRIVATE);
                        liczbaPompek = Integer.parseInt(sharedLiczbaPowtorzen.getString("liczbaPowtorzenPompek", ""));

                        if (liczbaPompek < 5) {
                            startWorkoutSeriaTrzy(dzien, 3);

                        } else if (liczbaPompek > 5 && liczbaPompek <= 10) {

                        } else if (liczbaPompek > 10 && liczbaPompek <= 20) {

                        } else if (liczbaPompek > 20 && liczbaPompek <= 25) {

                        } else if (liczbaPompek > 25 && liczbaPompek <= 30) {

                        } else if (liczbaPompek > 30 && liczbaPompek <= 35) {

                        } else if (liczbaPompek > 40 && liczbaPompek <= 45) {

                        } else if (liczbaPompek > 45 && liczbaPompek <= 50) {

                        } else if (liczbaPompek > 50 && liczbaPompek <= 55) {

                        } else if (liczbaPompek > 55 && liczbaPompek <= 60) {

                        } else if (liczbaPompek > 60) {

                        }
                    }
                }.start();
            }

        });
        liczbaUkonczonychPompek += liczbaPompekwSerii;
    }

    private void startWorkoutSeriaTrzy(int dzien, int liczbaPompekwSerii) {
        bStart.setVisibility(View.VISIBLE);
        tvInfo.setText("");
        tvTimerAndPushups.setText(R.string.ukonczono);
        tvTimerAndPushups.setTextColor(Color.GRAY);
        bEndSerie.setVisibility(View.GONE);

        Log.e("liczbaUkonczonychPompek", "" + liczbaUkonczonychPompek);
        String dayOfTraining = DateFormat.getDateInstance().format(new Date());

        CwiczenieModel cwiczenieModel = new CwiczenieModel(liczbaUkonczonychPompek, "" + dayOfTraining);

        databaseHandler.addPompki(cwiczenieModel);

    }
}