package com.staszekalcatraz.myfitapplication;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.staszekalcatraz.myfitapplication.Pompki.PompkiActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends Activity {

    @OnClick(R.id.bPompki)
    public void openPompki() {
        startActivity(new Intent(MainActivity.this, PompkiActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);
    }
}
