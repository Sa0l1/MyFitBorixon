package com.staszekalcatraz.myfitapplication.DatabaseHandler;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by krzysztofm on 23.01.2018.
 */

public class DatabaseHandler extends SQLiteOpenHelper {

    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "MyFitDatabase";

    // Contacts table name
    private static final String TABLE_POMPKI = "pompki";
    private static final String TABLE_BRZUSZKI = "brzuszki";
    private static final String TABLE_DIPY = "dipy";
    private static final String TABLE_PRZYSIADY = "przysiady";
    private static final String TABLE_PODCIAGNIECIA = "podciagniecia";

    // Contacts Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_NAZWA_CWICZENIA = "nazwa_cwiczenia";
    private static final String KEY_POWTORZENIA_CWICZENIA = "powtorzeniaCwiczenia";
    private static final String KEY_DATA_CWICZENIA = "dataCwiczenia";

    private CwiczenieModel cwiczeniaModel;


    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String CREATE_POMPKI_TABLE = "CREATE TABLE " + TABLE_POMPKI + "("
                + KEY_ID + " INTEGER PRIMARY KEY,"
                + KEY_NAZWA_CWICZENIA + " TEXT,"
                + KEY_POWTORZENIA_CWICZENIA + " TEXT,"
                + KEY_DATA_CWICZENIA + " TEXT"
                + ")";
        String CREATE_BRZUSZKI_TABLE = "CREATE TABLE " + TABLE_BRZUSZKI + "("
                + KEY_ID + " INTEGER PRIMARY KEY,"
                + KEY_NAZWA_CWICZENIA + " TEXT,"
                + KEY_POWTORZENIA_CWICZENIA + " TEXT,"
                + KEY_DATA_CWICZENIA + " TEXT"
                + ")";
        String CREATE_DIPY_TABLE = "CREATE TABLE " + TABLE_DIPY + "("
                + KEY_ID + " INTEGER PRIMARY KEY,"
                + KEY_NAZWA_CWICZENIA + " TEXT,"
                + KEY_POWTORZENIA_CWICZENIA + " TEXT,"
                + KEY_DATA_CWICZENIA + " TEXT"
                + ")";
        String CREATE_PRZYSIADY_TABLE = "CREATE TABLE " + TABLE_PRZYSIADY + "("
                + KEY_ID + " INTEGER PRIMARY KEY,"
                + KEY_NAZWA_CWICZENIA + " TEXT,"
                + KEY_POWTORZENIA_CWICZENIA + " TEXT,"
                + KEY_DATA_CWICZENIA + " TEXT"
                + ")";
        String CREATE_PODCIAGNIECIA_TABLE = "CREATE TABLE " + TABLE_PODCIAGNIECIA + "("
                + KEY_ID + " INTEGER PRIMARY KEY,"
                + KEY_NAZWA_CWICZENIA + " TEXT,"
                + KEY_POWTORZENIA_CWICZENIA + " TEXT,"
                + KEY_DATA_CWICZENIA + " TEXT"
                + ")";

        sqLiteDatabase.execSQL(CREATE_POMPKI_TABLE);
        sqLiteDatabase.execSQL(CREATE_BRZUSZKI_TABLE);
        sqLiteDatabase.execSQL(CREATE_DIPY_TABLE);
        sqLiteDatabase.execSQL(CREATE_PRZYSIADY_TABLE);
        sqLiteDatabase.execSQL(CREATE_PODCIAGNIECIA_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_POMPKI);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_BRZUSZKI);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_DIPY);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_PRZYSIADY);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_PODCIAGNIECIA);

        // Create tables again
        onCreate(sqLiteDatabase);
    }

    public void addPompki(CwiczenieModel cwiczenieModel) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_NAZWA_CWICZENIA, cwiczenieModel.getNazwaCwiczenia());
        values.put(KEY_POWTORZENIA_CWICZENIA, cwiczenieModel.getPowtorzeniaCwiczenia());
        values.put(KEY_DATA_CWICZENIA, cwiczenieModel.getDataCwiczenia());

        // Inserting Row
        db.insert(TABLE_POMPKI, null, values);
        db.close(); // Closing database connection
    }

    public void addBrzuszki(CwiczenieModel cwiczenieModel) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_NAZWA_CWICZENIA, cwiczenieModel.getNazwaCwiczenia());
        values.put(KEY_POWTORZENIA_CWICZENIA, cwiczenieModel.getPowtorzeniaCwiczenia());
        values.put(KEY_DATA_CWICZENIA, cwiczenieModel.getDataCwiczenia());

        // Inserting Row
        db.insert(TABLE_BRZUSZKI, null, values);
        db.close(); // Closing database connection
    }

    public void addDipy(CwiczenieModel cwiczenieModel) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_NAZWA_CWICZENIA, cwiczenieModel.getNazwaCwiczenia());
        values.put(KEY_POWTORZENIA_CWICZENIA, cwiczenieModel.getPowtorzeniaCwiczenia());
        values.put(KEY_DATA_CWICZENIA, cwiczenieModel.getDataCwiczenia());

        // Inserting Row
        db.insert(TABLE_DIPY, null, values);
        db.close(); // Closing database connection
    }

    public void addPrzysiady(CwiczenieModel cwiczenieModel) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_NAZWA_CWICZENIA, cwiczenieModel.getNazwaCwiczenia());
        values.put(KEY_POWTORZENIA_CWICZENIA, cwiczenieModel.getPowtorzeniaCwiczenia());
        values.put(KEY_DATA_CWICZENIA, cwiczenieModel.getDataCwiczenia());

        // Inserting Row
        db.insert(TABLE_PRZYSIADY, null, values);
        db.close(); // Closing database connection
    }

    public void addPodciagniecia(CwiczenieModel cwiczenieModel) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_NAZWA_CWICZENIA, cwiczenieModel.getNazwaCwiczenia());
        values.put(KEY_POWTORZENIA_CWICZENIA, cwiczenieModel.getPowtorzeniaCwiczenia());
        values.put(KEY_DATA_CWICZENIA, cwiczenieModel.getDataCwiczenia());

        // Inserting Row
        db.insert(TABLE_PODCIAGNIECIA, null, values);
        db.close(); // Closing database connection
    }

    // liczba rekordow w bazie pompek
    public int getPompkiCount() {
        String countQuery = "SELECT  * FROM " + TABLE_POMPKI;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);

        if (cursor != null)
            cursor.moveToFirst();
        cursor.close();

        return cursor.getCount();
    }

    // pobranie pojedynczego rekordu z bazy
    public CwiczenieModel getSingleArchiveTimeRecord(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_POMPKI, new String[]{
                        KEY_NAZWA_CWICZENIA, KEY_POWTORZENIA_CWICZENIA, KEY_DATA_CWICZENIA}, KEY_ID + "=?",
                new String[]{String.valueOf(id)}, null, null, KEY_ID, null);

        if (cursor != null) {
            cursor.moveToFirst();

            cwiczeniaModel = new CwiczenieModel(
                    "" + cursor.getString(0),
                    cursor.getInt(1),
                    "" + cursor.getString(2));
        }

        return cwiczeniaModel;
    }

    // Getting All
    public List<CwiczenieModel> getAllPompki() {
        List<CwiczenieModel> cwiczeniePompkiList = new ArrayList<CwiczenieModel>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_POMPKI;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                CwiczenieModel cwiczenieModel = new CwiczenieModel();
                cwiczenieModel.setId(Integer.parseInt(cursor.getString(0)));
                cwiczenieModel.setNazwaCwiczenia(cursor.getString(1));
                cwiczenieModel.setPowtorzeniaCwiczenia(cursor.getInt(2));
                cwiczenieModel.setDataCwiczenia(cursor.getString(3));
                // Adding contact to list
                cwiczeniePompkiList.add(cwiczenieModel);
            } while (cursor.moveToNext());
        }

        // return contact list
        return cwiczeniePompkiList;
    }

}
