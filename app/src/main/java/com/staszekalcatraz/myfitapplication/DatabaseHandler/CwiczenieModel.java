package com.staszekalcatraz.myfitapplication.DatabaseHandler;

/**
 * Created by krzysztofm on 23.01.2018.
 */

public class CwiczenieModel {
    private int id;
    private String nazwaCwiczenia;
    private int powtorzeniaCwiczenia;
    private String dataCwiczenia;

    public CwiczenieModel() {
    }

    public CwiczenieModel(String nazwaCwiczenia, int powtorzeniaCwiczenia, String dataCwiczenia) {
        this.nazwaCwiczenia = nazwaCwiczenia;
        this.powtorzeniaCwiczenia = powtorzeniaCwiczenia;
        this.dataCwiczenia = dataCwiczenia;
    }

    public CwiczenieModel(int id, String nazwaCwiczenia, int powtorzeniaCwiczenia, String dataCwiczenia) {
        this.id = id;
        this.nazwaCwiczenia = nazwaCwiczenia;
        this.powtorzeniaCwiczenia = powtorzeniaCwiczenia;
        this.dataCwiczenia = dataCwiczenia;
    }

    public CwiczenieModel(int powtorzeniaCwiczenia, String dataCwiczenia) {
        this.powtorzeniaCwiczenia = powtorzeniaCwiczenia;
        this.dataCwiczenia = dataCwiczenia;
    }

    public int getId() {
        return id;
    }

    public CwiczenieModel setId(int id) {
        this.id = id;
        return this;
    }

    public String getNazwaCwiczenia() {
        return nazwaCwiczenia;
    }

    public CwiczenieModel setNazwaCwiczenia(String nazwaCwiczenia) {
        this.nazwaCwiczenia = nazwaCwiczenia;
        return this;
    }

    public int getPowtorzeniaCwiczenia() {
        return powtorzeniaCwiczenia;
    }

    public CwiczenieModel setPowtorzeniaCwiczenia(int powtorzeniaCwiczenia) {
        this.powtorzeniaCwiczenia = powtorzeniaCwiczenia;
        return this;
    }

    public String getDataCwiczenia() {
        return dataCwiczenia;
    }

    public CwiczenieModel setDataCwiczenia(String dataCwiczenia) {
        this.dataCwiczenia = dataCwiczenia;
        return this;
    }
}
